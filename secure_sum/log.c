#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <stdarg.h>

#include "mytypes.h"
#include "colors.h"
#include "log.h"

/* classical magic to convert defines to strings */
#define xstr(s) str(s)
#define str(s) #s

/* static var. section initializes with 0 as default */
static lld g_log_level;
static FILE *g_log_stream;


static int init_log_stream(const char *log_filename)
{
	if (!log_filename) {
		g_log_stream = stderr;
		return 0;
	}
	
	g_log_stream = fopen(log_filename, "w");
	return !g_log_stream;
}


int log_ini(const char *log_filename, const lld log_level)
{
	int rc = 0;
	g_log_level = log_level;

	rc = init_log_stream(log_filename);
	return rc;
}


int log_fini(void)
{
	int rc = 0;
	if (!g_log_stream)
		return rc;
	
	/* close stderr isn't an expected effect */
	if (g_log_stream != stderr)
		rc = fclose(g_log_stream);
	return rc;
}


static const char *get_log_level_str(const int log_level)
{
	switch (log_level) {
	case LOG_ABORT:
		return ANSI_COLOR_MAGENTA str(LOG_ABORT) ANSI_COLOR_RESET;
	case LOG_ERROR:
		return ANSI_COLOR_RED str(LOG_ERROR) ANSI_COLOR_RESET;
	case LOG_INFO:
		return ANSI_COLOR_YELLOW str(LOG_INFO) ANSI_COLOR_RESET;
	case LOG_DEBUG:
	default:
		return ANSI_COLOR_GREEN str(LOG_DEBUG) ANSI_COLOR_RESET;
	}
}


static const char *get_time(time_t *rawtime)
{
	/* we need external rawtime as returning str is inherent */
	time (rawtime);
  	struct tm *timeinfo = localtime(rawtime);
	char *str = asctime(timeinfo);
	str[24] = '\0'; /* bad hack; we don't need newline character */
	return str;
}


void log_write(const int log_level, const char *fmt, ...)
{
	assert(g_log_stream != 0);

	if (g_log_level <= log_level)
		return;

	time_t rawtime;
	fprintf(g_log_stream, "%s [%s] : ", 
		get_time(&rawtime), get_log_level_str(log_level));

	va_list va;
	va_start(va, fmt);
	vfprintf(g_log_stream, fmt, va);
	va_end(va);

	fprintf(g_log_stream, "\n");

	if (log_level <= LOG_ABORT) {
		log_fini();
		exit(EXIT_FAILURE);
	}
}
