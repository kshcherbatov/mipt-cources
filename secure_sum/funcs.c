#include <limits.h>
#include "mytypes.h"
#include "funcs.h"
#include "log.h"

#define GCC_VERSION (__GNUC__ * 10000 \
                     + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)


bool secure_sum(lld a, lld b, lld *res)
{
	log_write(LOG_DEBUG, 
		"Called secure_sum(a = %lld, b = %lld, res = [%p])", a, b, res);

#if GCC_VERSION > 5000 
	log_write(LOG_DEBUG, "Current compiler has __builtin_saddll_overflow");
	return __builtin_saddll_overflow(a, b, res);
#else	
	log_write(LOG_DEBUG, "Current compiler has no __builtin_saddll_overflow. Use own construction.");
	*res = a + b;
	return ((b > 0 && a > LLONG_MAX - b)          /*  overflow  */
		|| (b < 0 && a < LLONG_MIN - b));     /* underflow  */
#endif 
}
