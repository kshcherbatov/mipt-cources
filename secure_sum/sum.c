#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "log.h"
#include "mytypes.h"
#include "funcs.h"

static void usage(const char *prg_name, const char *err_msg)
{
	if (err_msg)
		printf("%s\n", err_msg);

	printf("Usage: %s [ARGUMENTS] NUMBER NUMBER\n"
		"Available arguments:\n"
		"   -f,--log_file  FILENAME    log file (optional, default stderr stream)\n"
		"   -d,--log_level NUMBER      log level (optional, default 0)\n\n",
		prg_name);
}

static int parce_number(const char *str, lld *res)
{
	char *endptr = 0;
	errno = 0;
	*res = strtoll(str, &endptr, 10);
	return (endptr == str || *endptr != '\0' 
		|| errno == ERANGE);
}

static int parce_prg_options(int argc, char *argv[])
{
	const char *prg_name = argv[0];
	const char *log_filename = NULL;
	lld log_level = 0;

	const struct option long_options[] = {
		{"log_file",  required_argument, 0,  'f' },
		{"log_level", required_argument, 0,  'd' },
		{NULL,        0,                 0,   0  }
	};
	const char *short_options = "f:d:";
	
	int ch = 0; 
	while ((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1) {
		switch (ch) {
		case 'd':
			if (parce_number(optarg, &log_level)) {
				printf("Invalid log_level argument '%s'!\n", optarg);
				usage(prg_name, 0);
				return -1;
			}	
			break;
		case 'f':
			log_filename = optarg;
			break;
		default:
			usage(prg_name, "Unknown argument");
			return -1;
		}
	}
	
	if (log_ini(log_filename, log_level)) {
		printf("Can't initialize log subsystem!\n");
		return -1;
	}
	LOG_STARTUP(prg_name);

	return 0;
}

int main(int argc, char *argv[])
{	
	int rc = 0;

	if ((rc = parce_prg_options(argc, argv)))
		goto _cleanup;
	
	if (optind + 2 > argc) {
		usage(argv[0], "Too few input parameters!");
		rc = -1;
		goto _cleanup;
	}	
	
	lld a, b;
	if (parce_number(argv[optind], &a)
		|| parce_number(argv[optind + 1], &b)) {
		char buf[1024];
		snprintf(buf, 1024, "Can't parce numeric argument : %s", strerror(errno));
		usage(argv[0], buf);
		rc = -1;
		goto _cleanup;
	}
	log_write(LOG_DEBUG, "Readed numbers %lld %lld from console.", a, b);
	
	lld res;	
	if (secure_sum(a, b, &res)) {
		printf("Numbers are too big! Overflow!\n");
		rc = -1;
		goto _cleanup;
	}

	printf("Result is %lld\n", res);

	log_write(LOG_DEBUG, "Now I'm going to exit");
	
_cleanup:	
	log_fini();
	return rc;
}
