#include "mytypes.h"

enum LOG_LEVELS {
	LOG_ABORT = 0,
	LOG_ERROR = 1,
	LOG_INFO = 2,
	LOG_DEBUG = 3,
	LOG_LEVELS = 4,
};

int log_ini(const char *log_filename, const lld log_level);
int log_fini(void);
void log_write(const int log_level, const char *fmt, ...);

#define LOG_STARTUP(prgname) \
	 log_write(LOG_INFO, "Started %s (%s) {Last build %s %s}\n", prgname, __FILE__, __DATE__, __TIME__ );
